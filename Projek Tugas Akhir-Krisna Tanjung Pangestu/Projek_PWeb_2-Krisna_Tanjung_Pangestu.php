<html>
<head>
        <meta charset="utf-8">
        <meta name="Projek" content="width=device-width, initial-scale=1">
        <title>Projek PWeb 2-Krisna Tanjung Pangestu</title>
        <link rel="stylesheet" type="text/css" href="Projek2.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
    </head>
    <body>
        <header>
          <div class="header">
            <div class="menu">
            <h1><a href="">Projek Tugas Akhir</a></h1>
             <ul>
              <li><a href="#awal">Home</a></li>
              <li><a href="#content3">About Me</a></li>
              <li><a href="#content4">Skills</a></li>
              <li><a href="#footer">Contact</a></li>
              <li><a href="DataPengunjung.php">Data Pengunjung</a></li>
              <li><a href="Daftaranggota.php">Daftar</a></li>
             </ul>
            </div>
          </div>
          <hr>
        </header>
        <main>
            <div class="content1">
                <div class="foto">
                    <img src="Foto Krisna.jpg">
                </div>
            </div>
            <div class="content2">
                <div class="biodata">
                    <table>
                        <tr>
                            <td>Nama</td>
                            <td>: Krisna Tanjung Pangestu</td>
                        </tr>
                        <tr>
                            <td>Tempat/Tanggal Lahir</td>
                            <td>: Sekayu,06 September 2002</td>
                        </tr>
                        <tr>
                            <td>Agama</td>
                            <td>: Islam</td>
                        </tr>
                        <tr>
                            <td>Universitas</td>
                            <td>: Ahmad Dahlan Yogyakarta</td>
                        </tr>
                        <tr>
                            <td>NIM</td>
                            <td>: 2000018426</td>
                        </tr>
                        <tr>
                            <td>Fakultas</td>
                            <td>: Teknik Industri</td>
                        </tr>
                        <tr>
                            <td>Program Studi</td>
                            <td>: Teknik Informatika</td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>: Jl.Randik Rt.01/Rw.01 Kel.Serasan Jaya Kec.Sekayu Kab.Musi Banyuasin</td>
                        </tr>
                    </table>
                    </div>
                </div>
                <div id="content3">
                    <div class="perkenalan">
                        <p>Halo teman-teman semua, perkenalkan nama saya Krisna Tanjung Pangestu, saya lahir di Sekayu tanggal 06 September 2002 Saya anak ke 3 dari 3 bersaudara. Selamat datang di website saya ini,<br> saya adalah salah satu mahasiswa di Universitas Ahamad Dahlan Yogyakarta Fakultas Teknik Industri Program Studi Teknik Informatika.<br>NIM saya 2000018426, sebelum saya masuk Universitas Ahmad Dahlan ini saya bersekolah di SMA NEGERI 1 SEKAYU jurusan IPA.<br>Kegemaran saya adalah berlari, sering sekali saya berlari pada waktu sore ataupun pagi karena menurut saya berlari dapat menyehatkan tubuh.</p>
                    </div>
                </div>
                <div id="content4">
                    <h3>Skills</h3>
                <div class="skill-bar">
                  <label>HTML</label>
                  <div class="skills">
                     <div class="html">80%</div>
                  </div>
                  <label>CSS</label>
                <div class="skills">
                     <div class="css">70%</div>
                  </div>
                  <label>JavaScript</label>
                <div class="skills">
                     <div class="js">50%</div>
                 </div>
                 <label>PHP</label>
                <div class="skills">
                      <div class="php">40%</div>
                 </div>
           </div>
        </div>
        </main>
        <hr>
        <footer>
       <div id="footer">
       <div class="footer1">
       <div class="kolom1">
                  <h4>Alamat</h4>
                  <p>Jl.Randik Rt.01/Rw.01 Provinsi Sumatera Selatan</p>
              </div>
              <div class="kolom2">
                <h4>Email</h4>
                <p>krisnatp06@gmail.com</p>
            </div>
            <div class="kolom3">
                <h4>Telpon/HP</h4>
                <p>0812 8633 6151</p>
            </div>
            <div class="kolom3">
            <a href="https://www.facebook.com/krisnatanjung.pangestu/"><img src="facebook logo.png" width="150px;"></a>
            </div>
            <div class="kolom2">
            <a href="https://www.instagram.com/" ><img src="Instagram Logo.png" width="80px" height="80px"></a>
            </div>
            <div class="kolom1">
            <a href="https://www.linkedin.com/in/krisna-tanjung-pangestu-9bb581203/"><img src="Linkedin logo.png" width="120px" ></a>
            </div>
       </div>
       <div class="footer2">
          <div class="copy">
              <small>copyright &copy; 2021 - Krisna Tanjung Pangestu</small>
          </div>
      </div>
    </div>
        </footer>
    </body>
</html>